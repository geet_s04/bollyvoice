<?php
if($_SERVER['REQUEST_METHOD'] == "POST"){
    extract($_POST);
    include '../config.php';
    $id = $_POST['id'];
    $sql=mysqli_query($conn,"DELETE FROM work_diary where id=$id ");
    
    if ($sql) {
            header('Content-type: application/json');
            echo json_encode(array("statusCode"=>200, "result"=>"Deleted Successfully"));
    } 
    else {
        header('Content-type: application/json');
        echo json_encode(array("statusCode"=>201,"result"=>'something went wrong'));
    }
    mysqli_close($conn);
}else{
    $json = array("statusCode" => 201, "info" => "Request method not accepted!");
    header('Content-type: application/json');
    echo json_encode($json);
}
?>